# Project PlanYourWork

![](/images/Planyourwork.png)
PlanYourWork is online planer app inspired by [Trello](https://trello.com). You can create multiple projects and add members to a project. Work with your teammates on multiple boards at a time. 
<br>
Divide work between your teammates by assigning them to each task in list. You can customise each card by adding labels, cover, due date and checklists. The app keeps track of your finished tasks in checklists and informs you if your task is overdue.

PlanYourWork was developed using Angular11 framework and Bootstrap library on frontend, MongoDB, Express.js and Node.js on backend.

<h3>Demo video</h3>
<hr>
[Demo](https://drive.google.com/drive/u/0/folders/1yHw6SVAjsmnrjtGPCne-SWnikxjj5O7A)
<h3>Build procedure</h3>
<hr>
[Build](https://gitlab.com/matfpveb/projekti/2020-2021/07-PlanYourWork/-/wikis/Build-procedure)
<h3>Persistent data</h3>
<hr>
[Data](https://gitlab.com/matfpveb/projekti/2020-2021/07-PlanYourWork/-/wikis/Persistent-data)
<h3>Weekly reports</h3>
<hr>
[Reports](https://gitlab.com/matfpveb/projekti/2020-2021/07-PlanYourWork/-/wikis/Reports)

## Developers

- [Milos Milakovic, 152/2017](https://gitlab.com/Milak9)
- [Marija Lakic, 84/2017](https://gitlab.com/marijal74)
- [Ana Nikacevic, 272/2017](https://gitlab.com/AnaNika)
- [Milica Radojicic, 131/2017](https://gitlab.com/milicar7)
