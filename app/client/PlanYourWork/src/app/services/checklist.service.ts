import { MessageService } from './message.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs';
import { switchMap, tap } from 'rxjs/operators';

import { Checklist } from '../models/checklist.model';

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {
  private checklists: Observable<Checklist[]>;
  private readonly checklistsUrl = 'http://localhost:5000/api/checklists/';

  constructor(private http: HttpClient) {
    this.refreshChecklists();
  }

  private refreshChecklists(): Observable<Checklist[]>{
    this.checklists = this.http.get<Checklist[]>(this.checklistsUrl);
    return this.checklists;
  }

  public getChecklists(): Observable<Checklist[]>{
    return this.checklists;
  }

  public getChecklistById(id: string): Observable<Checklist> {
    return this.http
      .get<Checklist>(this.checklists + id);
  }

  public addChecklist(checklistTitle: string): Observable<Checklist> {
    const body = {
      title: checklistTitle
    };

    return this.http
      .post<Checklist>(this.checklistsUrl, body);
  }


  public changeChecklistTitle(checklistId: string, checklistTitle: string): Observable<Object> {
    const body = {
      title: checklistTitle
    };

    return this.http.patch(this.checklistsUrl + checklistId, body);

  }

  public removeChecklistById(id: string): Observable<Checklist[]> {
    return this.http.delete(this.checklistsUrl + id).pipe(
      switchMap(() => this.refreshChecklists())
    );
  }

  public addChecklistItem(checklistId : string, itemName : string){

    const body = {
      name : itemName,
    }
    return this.http.post(this.checklistsUrl + checklistId + "/add_item", body);
  }

  public deleteItem(checklistId : string, itemName : string){

    const body = {
      itemName : itemName
    }
    return this.http.patch(this.checklistsUrl + checklistId + "/delete_item", body);
  }
  public checkItem(checklistId : string, name :string, checked :boolean){
    const body ={
      name : name,
      checked : checked
    }
    return this.http.patch(this.checklistsUrl + checklistId + "/update_checked", body);
  }
  public hideItems(checklistId : string, hideCheckedItems : boolean, hideOrShow : string){
    const body ={
      hideCheckedItems : hideCheckedItems,
      hideCheckedItemsText : hideOrShow
    }
    return this.http.patch(this.checklistsUrl + checklistId + "/hide_items", body);
  }
}
