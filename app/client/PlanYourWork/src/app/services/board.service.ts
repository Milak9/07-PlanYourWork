import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { BoardModel } from '../models/board.model';
@Injectable({
  providedIn: 'root'
})
export class BoardService {

  private readonly boardsSubject: Subject<BoardModel[]> = new Subject<BoardModel[]>();
  public boards: Observable<BoardModel[]> = this.boardsSubject.asObservable();

  public readonly addBoardSubject: Subject<BoardModel> = new Subject<BoardModel>();
  public addBoard: Observable<BoardModel> = this.addBoardSubject.asObservable();

  private readonly boardsUrl = "http://localhost:5000/api/board/";
  private readonly projectsUrl = "http://localhost:5000/projects/";

  public currentProjectId: String;
  public currentBoardId: String;

  constructor(private http: HttpClient) {
   // this.refreshBoards();
  }

  public refreshBoards(projectId: String): Observable<BoardModel[]> {
    this.http.get<BoardModel[]>(this.projectsUrl+projectId+"/boards").subscribe(boards => {
      this.boardsSubject.next(boards);
    });

    return this.boards;
  }
  public getBoardsForAProject(projectId: String) : Observable<BoardModel[]>{

  return  this.http.get<BoardModel[]>(this.projectsUrl+projectId+"/boards");
  }
  public getBoard(boardId: string): Observable<BoardModel> {
    return this.http.get<BoardModel>(this.boardsUrl + boardId);
  }

  public updateBoard(boardId: string, boardName: string, list: string): Observable<Object> {
    const body = {
      name: boardName,
      list: list
    };

    return this.http.put(this.boardsUrl + boardId, body);
  }

  public createNewBoard(boardName: string, projectId: string): Observable<BoardModel> {

    const project = projectId === undefined ? this.currentProjectId : projectId;

    const body = {
      "name": boardName,
      "projectId": project
    };

    return this.http.post<BoardModel>(this.projectsUrl + project, body);
  }

  public deleteBoard(projectId: string,boardId: string): Observable<Object> {
    const project = projectId === undefined ? this.currentProjectId : projectId;
    return this.http.delete(this.projectsUrl + project + "/" + boardId);
  }
}
