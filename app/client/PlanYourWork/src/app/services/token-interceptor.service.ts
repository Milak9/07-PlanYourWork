import { Injectable } from '@angular/core';
import { HttpRequest,  HttpHandler, HttpInterceptor,HttpErrorResponse} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError,  switchMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: AuthService,private router:Router) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
    if (localStorage.getItem("JWT_TOKEN")) {
      request = this.addToken(request, localStorage.getItem("JWT_TOKEN"));
    }

    return next.handle(request).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && error.status === 500 && error.error.error.message === "jwt expired") {
        return this.handle401Error(request, next);
      }
      else if(error instanceof HttpErrorResponse && error.status === 500 && error.error.error.message === "refresh invalid!" ){
        window.alert("Your session inspired. Log in again.");
        this.router.navigate(['../login'])
        this.auth.removeTokens();
      }
      return throwError(error);

    }));

  }
  private addToken(request: HttpRequest<any>, token: string) {
    return request.clone({
      setHeaders: {
        'Authorization': "Bearer "+ token
      }
    });
  }
  private handle401Error(request: HttpRequest<any>, next: HttpHandler) {
      return this.auth.refreshToken().pipe(
        switchMap((tokens: any) => {
           this.auth.setToken(tokens.accessToken)
          return next.handle(this.addToken(request, tokens.accessToken));
        })
        );
  }
}
