import { User } from './../models/user.model';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import {Project} from '../models/project.model'

@Injectable({
  providedIn: 'root'
})
export class MessageService {
  private msg = new Subject<any>();
  constructor() { }

  sendMessage(list: String, canUpdate: boolean, text = "") {
    this.msg.next({ whichList: list,
                    update: canUpdate,
                    text : text});
  }
  clearMessages() {
    this.msg.next();
  }

  getMessage(): Observable<any> {
    return this.msg.asObservable();
  }
}
