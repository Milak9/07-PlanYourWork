import { BoardService } from './board.service';
import { MessageService } from './message.service';
import { Injectable } from '@angular/core';
import { Observable,throwError, Subject  } from 'rxjs';
import { Project } from '../models/project.model';
import { User} from '../models/user.model'
import { BoardModel} from '../models/board.model';
import { HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import { tap, switchMap } from 'rxjs/operators'
import { EventEmitterService } from './event-emitter.service';
import { AuthService } from './auth.service';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  private projectsListEnabled : boolean = false;
  private projects:Array<Project> = [];
  private boards:Array<BoardModel> = [];
  private teamMembers:Array<String> = [];
  private readonly projectsUrl="http://localhost:5000/projects/";
  private readonly usersUrl="http://localhost:5000/api/users/" ;


  public readonly userSubject: Subject<any> = new Subject<any>();
  public displayUser: Observable<any> = this.userSubject.asObservable();

  constructor(private http:HttpClient,
              private messageService:MessageService,
              private eventEmitterService: EventEmitterService,
              private authService:AuthService,
              private boardService: BoardService,
              private toastrService:ToastrService) {
    this.boards= this.getBoards();
  }


  sendMessage(which : String, update: boolean, text = "") : void{
    this.messageService.sendMessage(which, update, text);
  }
  clearMessages( msg : String) : void{
    this.messageService.clearMessages();
  }

  public getProjects(){
    return this.projects;
  }

  public uploadProfilePic(username: String, file: File): any {
    const formData: FormData = new FormData();
    formData.append("file", file);
    const headers: HttpHeaders = new HttpHeaders();

    return this.http.patch(this.usersUrl + username + "/profile-image", formData, {headers});
  }
  public getProjectById(id: string): Observable<Project> {
    return this.http.get<Project>(this.projectsUrl + id);
  }
  public editProject(proj_id : String, body : any) : Observable<Object>{

    let req = this.http.put(this.projectsUrl + proj_id, body)
                  .pipe(
                    switchMap(() => {
                        return this.fetchProjects("editing project", this.authService.sendUserDataIfExists().username)
                    }));
    return req;
  }

  public fetchProjects(text : string, username:String):Observable<Project[]>{
    let req = this.fetchUserProjects(username).pipe(
      tap((data : Project[]) => {
      this.projects = data;
      if(this.projects.length!==0){
        let users = this.projects
                  .map((proj) => proj.team)
                  .concat()
                  .reduce((prev, curr) => prev.concat(curr));
        this.teamMembers = users.filter((user, i) => i===users.indexOf(user))
        .filter((user) => user!==this.authService.sendUserDataIfExists().username);
      }
      this.sendMessage('project', true, "fetch");
    }));
    return req;
  }

  public getBoards():Array<BoardModel>{
    return this.boards;
  }
  public getMembers():Array<String>{
    return this.teamMembers;
  }
  public setProjectsListEnabled():void{
    this.projectsListEnabled = !this.projectsListEnabled;
  }
  public getProjectsListEnabled():boolean{
    return this.projectsListEnabled;
  }
  public UpdateUser(username:String,newData:Object){
      return this.http.put<User>(this.usersUrl + username, newData);
  }
 public  createProject(projectName,projectMembers,projectDescription, boardName) : Observable<Object>{
  const bodyP = {

    "title": projectName,
    "description": projectDescription,
    "team": projectMembers,
    "name": boardName
  };

  this.projectsListEnabled= true;
  let responseProjects = this.http.post<any>(this.projectsUrl,bodyP)
                             .pipe(switchMap((data: any)=>{
                                this.boardService.currentProjectId = data.project_id;
                                this.boardService.currentBoardId = data.board_id;
                                const fetchedUser = this.authService.getUserFromToken(data.accessToken);
                                return this.fetchProjects("created a project",this.authService.sendUserDataIfExists().username)
                              }));
   return responseProjects;
  }
  ShowDiffrentUser(username:String,fullname:String,email:String, imgUrl){
    this.eventEmitterService.onFirstComponentButtonClick(username,fullname,email, imgUrl);
  }

  public getDiffrentUser(username:String) : Observable<Object>{

    let responseUsers = this.http.get<User>(this.usersUrl + username)
                                  .pipe(switchMap((user:User)=>{
      this.ShowDiffrentUser(user.username, user.fullname, user.email,user.imgUrl);

      return this.fetchProjects('after login, projects', user.username)
          .pipe(tap(() => {},
                    (error) =>{
                      this.toastrService.error(error.error.message, 'Error'); }));
    }));

    return responseUsers;

  }
  private fetchUserProjects(username : String){
    const res = this.http.get(this.usersUrl + username + "/projects");
    return res;
  }

  public leaveProject(proj_id : String) : Observable<Object>{
    let reqToRemoveProject = this.http.put(this.projectsUrl + proj_id + "/team/delete_member", {team_delete: [this.authService.sendUserDataIfExists().username]})
                                    .pipe(switchMap((data:any) => {
                                      this.authService.getUserFromToken(data.accessToken);
                                      return this.fetchProjects("leave project", this.authService.sendUserDataIfExists().username)
                                    }));
    return reqToRemoveProject;
  }
  public  getTeamMembers(projId : string) : Observable<Object>{
    return this.http.get(this.projectsUrl + projId + "/team");
  }
 public errorHandler(error:HttpErrorResponse){
    this.sendMessage('project', false, "error");
    return throwError(error.error.message || "Server error");
  }


}
