import { Injectable, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs/internal/Subscription';
@Injectable({
  providedIn: 'root'
})
export class EventEmitterService {
  invokeFirstComponentFunction = new EventEmitter();
  subsVar: Subscription;

  constructor() { }

  onFirstComponentButtonClick(username:String,fullname:String,email:String, imgUrl: String) {
    const showUser = {
      "username":username,
      "fullname": fullname,
      "email": email,
      "imgUrl": imgUrl
    }
    this.invokeFirstComponentFunction.emit(showUser);
  }
}
