import { switchMap, tap } from 'rxjs/operators';
import { BoardModel } from './../models/board.model';
import { BoardService } from './../services/board.service';
import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { ProfileService } from '../services/profile.service';

@Component({
  selector: 'app-board-nav-bar',
  templateUrl: './board-nav-bar.component.html',
  styleUrls: ['./board-nav-bar.component.css']
})

export class BoardNavBarComponent implements OnInit {

  closeModal: string;
  public modalForm: FormGroup;
  errorInvalidFormat: boolean = false;
  private activeSubscriptions : Subscription[] = [];
  private boardId: string;
 boards:BoardModel[];
  @Input()
  projectId: string;
  @Input()
 currentBoardId:string;

  constructor(private modalService: NgbModal,
              private formBuilder: FormBuilder,
              public boardService: BoardService,
              private authService: AuthService,
              private profileService: ProfileService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {


    const params = this.activatedRoute.paramMap
                      .pipe(switchMap(params => {
      this.boardId = params.get('boardId');
      this.projectId = params.get('projectId');

      return this.boardService.refreshBoards(this.projectId);
    })).subscribe();
    this.activeSubscriptions.push(params);

    this.modalForm = this.formBuilder.group({
      boardName: ['', [Validators.required, Validators.pattern('([a-zA-Z0-9-]+[ -]?[a-zA-Z0-9-]*)+')]]
    });
  }


  ngOnInit(): void {}

  ngOnDestroy() {
    this.activeSubscriptions.forEach((sub: Subscription) => {
      sub.unsubscribe()
    });
  }


  triggerModal(content) {
    this.modalService.open(content).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
      this.submitFormNewBoard(res);
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
      this.modalForm.reset();
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  checkForInput(): Object {
    return {
      "is-valid": this.modalForm.valid,
      "is-invalid": !this.modalForm.valid
    };
  }

  public submitFormNewBoard(data) {
    const boardName: string = data.boardName.trim();

    this.modalForm.reset();

    const sub = this.boardService.createNewBoard(boardName, this.projectId).subscribe((board: BoardModel) => {
      const id = board._id;
      this.router.navigate(['/project', this.projectId, id]);
      this.boardService.addBoardSubject.next(board);
    });

    this.activeSubscriptions.push(sub);
  }
  goBacktoProfile(){
    let subGoToProfile = this.profileService.getDiffrentUser(this.authService.sendUserDataIfExists().username).subscribe();
    this.activeSubscriptions.push(subGoToProfile);
    this.router.navigate(['/profile',this.authService.sendUserDataIfExists().username]);
  }


  public deleteBoard() {
    const sub = this.boardService.deleteBoard(this.projectId,this.boardId).subscribe(()=>{
      this.goBacktoProfile();
    });

    this.activeSubscriptions.push(sub);
  }

}
