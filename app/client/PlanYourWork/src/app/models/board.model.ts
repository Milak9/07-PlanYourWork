import { ListModel } from "./list.model";

export interface BoardModel {
  _id: string;
  name: string;
  lists: ListModel[] | string[];
}
