export class User {

 constructor(public _id: string,
             public username: String,
             public email: String,
             public fullname: string,
             public projects:Array<String>,
             public imgUrl : String) {}

}
