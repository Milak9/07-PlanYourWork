import { BoardModel } from './board.model';

export class Project {
  public _id:String;
  constructor(
     public title:String,
     public description:String,
     public team:Array<String>,
     public boards:Array<BoardModel>=null,
     public visibilityForBoards : boolean = false
) {
}
}

