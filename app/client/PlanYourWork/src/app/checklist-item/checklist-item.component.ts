import { Subscription } from 'rxjs';
import { ChecklistService } from './../services/checklist.service';
import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ChecklistComponent } from '../checklist/checklist.component';
import { checklistItem } from '../checklist/checklist.component';

@Component({
  selector: 'app-checklist-item',
  templateUrl: './checklist-item.component.html',
  styleUrls: ['./checklist-item.component.css']
})
export class ChecklistItemComponent implements OnInit, OnDestroy {

  static NB_INSTANCES: number = 0;
  itemId: string = 'item-' + ChecklistItemComponent.NB_INSTANCES++;

  @Input()
  item: checklistItem;

  @Input()
  checklist: ChecklistComponent;

  @Input()
  checklistId: string;

  @Input()
  hideChecked: boolean;

  private activeSubs :  Subscription[] = [];
  constructor(private checklistService : ChecklistService) {}

  ngOnInit(): void {}

  onCheckboxChange(): void{
    const isChecked = (document.getElementById(this.itemId) as HTMLInputElement).checked;
    if (isChecked){
      this.item.checked = true;
      this.checklist.changeProgress();
      this.checklist.updateHiddenItemsCount();
    }else{
      this.item.checked = false;
      this.checklist.changeProgress();
    }
    const sub = this.checklistService.checkItem(this.checklistId, this.item.name, this.item.checked)
                    .subscribe(() => this.checklist.changeProgress());
    this.activeSubs.push(sub);
  }

  onDeleteItem(): void{
    this.item.id = this.itemId;
    this.checklist.deleteItem(this.item.id, this.item.name);
  }

  ngOnDestroy(): void {
    this.activeSubs.forEach((sub) => sub.unsubscribe);
    this.activeSubs = [];
  }
}
