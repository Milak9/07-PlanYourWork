import { switchMap } from 'rxjs/operators';
import { ListModel } from './../models/list.model';
import { CardComponent } from './../card/card.component';
import { Card } from './../models/card.model';
import { CardService } from './../services/card.service';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { Component, Input, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { BoardComponent } from '../board/board.component';
import { ListService } from './../services/list.service';
import { MatDialog } from '@angular/material/dialog';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit, OnDestroy {

  @Input('listName')
  listName: string;

  tasks: Card[] = [];

  @Input('listId')
  listId: string;

  @Input('board')
  board: BoardComponent;

  private activeSubscriptions: Subscription[] = [];
  constructor(private listService: ListService,
              private cardService: CardService,
              private dialog: MatDialog,
              private toastrService: ToastrService) { }

  ngOnInit(): void {
    const sub = this.listService.getListById(this.listId).subscribe((list: ListModel) => {
      this.tasks = [];

      list.tasks.forEach((card: any) => {
          this.tasks.push({_id: card._id, listId: card.listId,
            title: card.title, description: card.description,
            members: card.members, labels: card.labels,
            dueDate: card.dueDate, completionStatus: card.completionStatus,
            fileUrls: card.fileUrls, hideAttachments: card.hideAttachments,
            hideAttachmentsText: card.hideAttachmentsText,  cover: card.cover, checklists: card.checklists});
        });
    });

    const subChange = this.cardService.card.subscribe((cardData: any) => {
      this.tasks.forEach((card: Card) => {
        if (card._id === cardData.cardId) {
          if (cardData.title)
            card.title = cardData.title;
          else if (cardData.cover)
            card.cover = cardData.cover;

          if (cardData.dueDate || cardData.dueDate === null)
            card.dueDate = cardData.dueDate;
          if (cardData.completionStatus)
            card.completionStatus = cardData.completionStatus;
        }
      });
    });

    this.activeSubscriptions.push(subChange);
    this.activeSubscriptions.push(sub);
  }

  ngOnDestroy(): void { this.activeSubscriptions.forEach(sub => sub.unsubscribe());}

  changeListName(event: Event): void {
    let newName = (event.target as HTMLInputElement).value.trim();

    if (this.listName === newName)
      return;

    if (newName === "")
    {
      (event.target as HTMLInputElement).value = this.listName;
      this.toastrService.error("You must provide name for the list", 'Error');
      return;
    }

    this.listName = newName;
    const sub = this.listService.changeListName(this.listId, this.listName).subscribe();
    this.activeSubscriptions.push(sub);
  }

  addTaskToTheList(event: Event): void {
    let taskName: string = (event.target as HTMLInputElement).value;

    if (taskName === "")
      return;

    (event.target as HTMLInputElement).value = "";

    let cardSub = this.cardService.createNewCard(taskName, this.listId)
                      .pipe(switchMap((card: Card) => {
      this.tasks.push(card);

      return this.listService.updateTasksInList(this.listId, this.tasks.map((task: Card) => task._id));

    })).subscribe();

    this.activeSubscriptions.push(cardSub);
  }

  onDeleteList(): void {this.board.deleteList(this.listId);}

  updateDataAfterDrop(event: CdkDragDrop<string[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(this.tasks, event.previousIndex, event.currentIndex);
      let taskUpdate = this.listService.updateTasksInList(this.listId, this.tasks.map((task: Card) => task._id)).subscribe();
      this.activeSubscriptions.push(taskUpdate);
    }
    else {

      let beforeTransferTasks: any =  event.previousContainer.data;
      let listPrevious: string = beforeTransferTasks[0].listId;

      let cardId = beforeTransferTasks[event.previousIndex]._id;
      transferArrayItem(event.previousContainer.data,
                        event.container.data,
                        event.previousIndex,
                        event.currentIndex);



      let tasksPrevious: string[] = [];

      event.previousContainer.data.forEach((value: any) => {
        tasksPrevious.push(value._id);
      });
      let previousListUpdate = this.listService.updateTasksInList(listPrevious, tasksPrevious).subscribe();
      this.activeSubscriptions.push(previousListUpdate);

      let tasksCurrent: string[] = [];
      let listCurrent: string = this.listId;
      event.container.data.forEach((value: any) => {
        tasksCurrent.push(value._id);
      });

      let currentListUpdate = this.listService.updateTasksInList(listCurrent, tasksCurrent).subscribe();
      this.activeSubscriptions.push(currentListUpdate);

      let updateCard = this.cardService.changeListIdOfCard(cardId, listCurrent).subscribe();
      this.activeSubscriptions.push(updateCard);
    }

  }
   deleteCard(card:Card){
     let cardIndex: number = this.tasks.findIndex((task) => task._id === card._id);

    if (cardIndex === -1) {
      return;
    }
    this.tasks.splice(cardIndex, 1);
    const body = {
      tasks : this.tasks.map((task:Card)=> task._id),
      card_id: card._id

      };
    const subDeleteCard =  this.cardService.deleteCard(card._id,this.listId).subscribe((data:any)=>{

   });
   this.activeSubscriptions.push(subDeleteCard);
  }

  openCard(cardId: string) {
    if(!this.cardService.showCard){
      this.cardService.showCard = !this.cardService.showCard;
      this.dialog.open(CardComponent, {
        data: { cardId: cardId,
                listId: this.listId,
                projectId: this.board.projectId,
                listName: this.listName
              },
        height: 'auto'
      });
    }
  }
  dueDateExists(checkDate:boolean){
    if (!checkDate)
      return {
        "emptyDate": true
      };
    else
      return {
        "hasDate": true
      };
  }

  checkDate(cardComplete: string) {
    if (cardComplete === 'complete')
      return {
        "success": true
      };
    else if (cardComplete === 'overdue')
      return {
        "danger": true
      };
    else
      return {
        "in-progress": true
      };
  }
}
