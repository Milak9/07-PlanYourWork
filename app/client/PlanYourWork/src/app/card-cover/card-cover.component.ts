import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';

@Component({
  selector: 'app-card-cover',
  templateUrl: './card-cover.component.html',
  styleUrls: ['./card-cover.component.css']
})
export class CardCoverComponent implements OnInit {

  @Input()
  addedCover: string;

  @Output()
  emitShowCover: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  emitCoverChosen: EventEmitter<string> = new EventEmitter<string>();

  public coverColors: string[];

  constructor() { this.coverColors = ['green', 'yellow', 'orange', 'red', 'purple', 'blue'];}

  ngOnInit(): void {}

  onCloseCover(): void{ this.emitShowCover.emit(false);}

  onCoverChosen(colorIndex: number): void{ this.emitCoverChosen.emit(this.coverColors[colorIndex]);}

  coverFound(chosenCover: string): boolean{
    if (this.addedCover === chosenCover){
      return true;
    }
    else{
      return false;
    }
  }

  onRemoveCover(): void{ this.emitCoverChosen.emit('none');}
}
