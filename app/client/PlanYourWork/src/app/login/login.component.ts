import { ProfileService } from './../services/profile.service';
import { Component, OnInit} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../models/user.model';
import { Subscription } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit  {


  public user:User;
  private activeSubscriptions : Array<Subscription> = [];
  public loginForm: FormGroup;
  fieldTextType: boolean;

  constructor(private formBuilder:FormBuilder,
              private router:Router,
              private profileService: ProfileService,
              private authService: AuthService,
              private toastrService: ToastrService) {
    this.loginForm = this.formBuilder.group({
      email:['',[Validators.required,Validators.email]],
      password:['',[Validators.required]]
    });
   }

  ngOnInit(): void {}

  ngOnDestroy() { this.activeSubscriptions.forEach((sub:Subscription) => {sub.unsubscribe()});}

  public get email(){ return this.loginForm.get('email');}

  public get password(){ return this.loginForm.get('password');}

  toggleFieldTextType() { this.fieldTextType = !this.fieldTextType;}

  public submitForm(){

    const data = {
      "email":this.email.value,
      "password":this.password.value
     };


    let loginSub = this.authService.loginUser(data)
    .subscribe((tokens:any) => {
     const fetchedUser = this.authService.getUserFromToken(tokens.accessToken,tokens.refreshToken)
     this.router.navigate(['/profile',fetchedUser.username]);
    },
    (error) => {
      this.toastrService.error(error.error.message, 'Error');
    }
    );
   this.activeSubscriptions.push(loginSub);


  }



}
