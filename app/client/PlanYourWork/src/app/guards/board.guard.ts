import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../services/auth.service';
import { ProfileService } from '../services/profile.service';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class BoardGuard implements CanActivate {
  constructor(private profileService:ProfileService, private authService:AuthService,private router:Router,private toastrService:ToastrService) {}
  canActivate(route: ActivatedRouteSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree{

    if(this.authService.loggedIn() && this.authService.sendUserDataIfExists().projects.includes(route.paramMap.get('projectId'))){
      return true;
    }
    else{
      if(!this.authService.loggedIn()){
        this.toastrService.error("Log in to access your projects and boards.", 'Error');
        return  this.router.createUrlTree(["../login"]);
      }
      else{
         this.toastrService.error("You are not apart of this project. Ask your colleagues to add you.",'Error');
         this.router.navigate(['']);

        return false;
     }
    }

  }

}
