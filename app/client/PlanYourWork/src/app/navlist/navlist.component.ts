import { MessageService } from './../services/message.service';
import { Subscription } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators, FormArray, FormControl} from '@angular/forms';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Project} from '../models/project.model';
import {BoardModel} from '../models/board.model';
import {ProfileService} from '../services/profile.service'
import {Router, ActivatedRoute} from '@angular/router';
import { AuthService } from '../services/auth.service';
import { BoardService } from '../services/board.service';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-navlist',
  templateUrl: './navlist.component.html',
  styleUrls: ['./navlist.component.css']
})
export class NavlistComponent implements OnInit {

  boardsListEnabled : boolean = false;
  changeTeam : boolean = false;
  searchText : String = "";
  searchGroup : FormGroup;
  public modalForm: FormGroup;
  closeModal: string;
  projects : Array<Project> = [];
  boards: Array<BoardModel> = [];
  teamMembers: Array<String> = [];
  private messages = [];
  private activeSubscriptions : Subscription[] = [];
  suggestions : any = [];
  sameProfile: boolean;

  constructor(private formBuilder:FormBuilder,
              private modalService: NgbModal,
              private profileService:ProfileService,
              private messageService:MessageService,
              private authService:AuthService,
              private boardService: BoardService,
              private router:Router,
              private activatedRoute: ActivatedRoute,
              private toastrService: ToastrService) {


   const authSub = this.activatedRoute.url.subscribe(url =>{
     if(this.authService.sendUserDataIfExists().username===url[1].path){ this.sameProfile =  true; }
      else { this.sameProfile = false;}
    });
    this.activeSubscriptions.push(authSub);

    this.projects = this.profileService.getProjects()

    let sub = this.messageService.getMessage().subscribe((msg) => {
        if(msg){
          this.messages.push(msg);
          if(msg.whichList == 'project' && msg.update){
            this.refreshProjectList();
          }
          else if(msg.whichList == 'boards' && msg.update){
            this.refreshBoardsList();
          }
          this.activeSubscriptions.push(sub);
        }
        else{
          this.messages = [];
        }
      }
    );

    this.searchGroup = this.formBuilder.group({
      searchText:['', [Validators.required]]
    });
    this.modalForm = this.formBuilder.group({
      updatedName:[''],
      updatedDescription: [''],
      updatedTeam: this.formBuilder.array([]),
      addedMembers: ['']
    });
  }
  private refreshBoardsList(){
    this.boards = this.profileService.getBoards();
    this.suggestions = [...this.projects.map((p) => p.title).concat(),
      ...this.teamMembers,
      ...this.boards.map((m) => m.name).concat()];
  }

  private refreshProjectList(){
    this.projects = this.profileService.getProjects();
    this.teamMembers = this.profileService.getMembers();
    this.suggestions = [...this.projects.map((p) => p.title).concat(),
      ...this.teamMembers,
      ...this.boards.map((m) => m.name).concat()];
  }

  sendMessage(update : boolean){ this.messageService.sendMessage('project', update);}

  clearMessages(update : boolean){ this.messageService.clearMessages();}
  ngOnInit(): void {}

  showProjects() : String{
    let res = this.profileService.getProjectsListEnabled() ? "collapse show" : "collapse";
    return (res);
  }


  showBoards(show:boolean) : String{ return (show ? "collapse show" : "collapse");}

  onCollapse(row : String) : void{
    if(`${row}` === "projects"){
      this.profileService.setProjectsListEnabled();
    }
    else if(`${row}` === "boards"){
      this.boardsListEnabled = !this.boardsListEnabled;
    }
  }

  onDeleteBoard( board : BoardModel): void{

    let boardIndex: number = this.boards.findIndex((existingBoard: BoardModel) =>
    existingBoard.name === board.name);
    if (boardIndex === -1) {
      return;
    }

    this.boards.splice(boardIndex, 1);
  }
  onLeaveProject( project : Project) :void{
    let projIndex: number = this.projects.findIndex((existingProject: Project) =>
    existingProject._id === project._id);
    if (projIndex === -1) {
      return;
    }
    const removedItem = (this.projects.splice(projIndex, 1))[0];

    let sub = this.profileService.leaveProject(project._id).subscribe();

    this.activeSubscriptions.push(sub);
  }

  onInputSearchText() : void{
    if (this.searchText.toLowerCase().startsWith(":user")){
      const userNames = this.teamMembers.sort();
      const regex = new RegExp(this.searchText.toLowerCase().substring(":user".length).trim() + ".?", 'i');
      this.suggestions = userNames.filter((name) => name.match(regex));
    }
    else if(this.searchText.toLowerCase().startsWith(":project")){
      const projectNames = this.projects.map((proj) => proj.title.toLowerCase());
      const regex = this.searchText.toLowerCase().substring(":project".length).trim() + ".?";
      this.suggestions = projectNames.filter((name) => name.match(regex));

    }
    else if(this.searchText.toLowerCase().startsWith(":board")){
      const boardNames = this.boards.map((board) => board.name.toLowerCase());
      const regex = this.searchText.toLowerCase().substring(":board".length).trim() + ".?";
      this.suggestions = boardNames.filter((name) => name.match(regex));
    }
    else{
      console.log(this.searchText);
    }
  }
  onSearch(data){ console.log(data);}

  public editProject(data){
    const modifiedProject = data.item._id;
    const updates = data.updates;
    updates.updatedTeam = updates.updatedTeam.filter((member) => member!==null);

    if(!updates.updatedName && !updates.updatedTeam.length &&
       !updates.addedMembers && updates.updatedDescription===data.item.description){
      return;
    }

    if(updates.addedMembers!==null && updates.addedMembers!==""){

      updates.addedMembers = updates.addedMembers.split(",").map((user)=>user.trim());
      updates.addedMembers = updates.addedMembers.filter((user) => user!==this.authService.sendUserDataIfExists().username);

      let diffMembers = data.item.team.filter((user) => user!==this.authService.sendUserDataIfExists().username);;
      for(let deleted of updates.updatedTeam){
        diffMembers = diffMembers.filter((user)=> user!==deleted);
      }
      for(let member of diffMembers){
        const index = updates.addedMembers.findIndex((user)=>user===member);
        if(index !== -1){
          updates.addedMembers.splice(index, 1);

        }

      }
      let bothActionsMembers = [];

      for(let deleted of updates.updatedTeam){
        const index = updates.addedMembers.findIndex((user) => user === deleted);
        if(index!==-1){
          bothActionsMembers.push(deleted);
        }
      }

      for(let member of bothActionsMembers){
        updates.updatedTeam = updates.updatedTeam.filter((user) => user!==member);
        updates.addedMembers = updates.addedMembers.filter((user) => user!==member);
      }
    }

    const body = {title: updates.updatedName ? updates.updatedName : data.item.title,
                  description: updates.updatedDescription ? updates.updatedDescription : data.item.description,
                  team_delete : updates.updatedTeam.length ? updates.updatedTeam : null,
                  team_add : updates.addedMembers ? updates.addedMembers : null
                  };

    let sub = this.profileService.editProject(modifiedProject, body).subscribe(() => {}, (error)=>{
      this.toastrService.error(error.error.message, 'Error');


    });


    this.activeSubscriptions.push(sub);
    this.modalForm.reset();

  }
  public updateTeamList(item : Project){ return item.team.filter((user) => user!==this.authService.sendUserDataIfExists().username);}

  triggerModal(content) {
    this.modalService.open(content).result.then((res) => {
      this.closeModal = `Closed with: ${res}`;
      this.editProject(res);
      this.changeTeam = false;
    }, (res) => {
      this.closeModal = `Dismissed ${this.getDismissReason(res)}`;
      this.modalForm.reset();
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  onUpdateTeam(){ this.changeTeam = !this.changeTeam;}

  onCheckboxChange(e){

    let checkArray: FormArray;
    checkArray = this.modalForm.get('updatedTeam') as FormArray

    if (e.target.checked) {
      checkArray.push(new FormControl(e.target.value));
    } else {
      let i: number = 0;
      checkArray.controls.forEach((item: FormControl) => {
        if (item.value == e.target.value) {
          checkArray.removeAt(i);
          return;
        }
        i++;
      });
    }

  }

  public onProfileChange(username:String){
    this.router.navigate(['/profile',username]);
    let sub = this.profileService.getDiffrentUser(username).subscribe();
    this.activeSubscriptions.push(sub);
  }
  public showBoardsForProject(item:Project){

    item.visibilityForBoards=!item.visibilityForBoards;

    if(item.visibilityForBoards){
    let sub = this.boardService.getBoardsForAProject(item._id).subscribe((projectBoards)=>{
      item.boards = projectBoards;
    });
    this.activeSubscriptions.push(sub);

  }

  }
  public goToBoard(project:Project ,board:BoardModel){
    let projIndex: Project = this.projects.find((existingProject: Project) =>
    existingProject._id === project._id);
    if (!projIndex) {
      return;
    }
    this.boardService.currentProjectId=projIndex._id;
    this.boardService.currentBoardId=board._id;
    this.router.navigate(['../../project', this.boardService.currentProjectId, this.boardService.currentBoardId]);
  }
}
