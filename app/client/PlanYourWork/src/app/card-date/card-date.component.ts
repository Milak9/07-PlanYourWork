import { Component, OnInit, Output, EventEmitter, Injectable } from '@angular/core';

@Component({
  selector: 'app-card-date',
  templateUrl: './card-date.component.html',
  styleUrls: ['./card-date.component.css'],
  providers: []
})
export class CardDateComponent implements OnInit {

  @Output()
  emitShowDate: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  emitSaveDate: EventEmitter<Date> = new EventEmitter<Date>();

  @Output()
  emitRemoveDate: EventEmitter<boolean> = new EventEmitter<boolean>();

  model: string;
  isoDate: string;

  constructor() { }

  ngOnInit(): void {}

  onSaveDate(): void{
    if(this.model){
      this.isoDate = this.model.split('-', 3).reverse().join('-');
      const date: Date = new Date(this.isoDate);
      this.emitSaveDate.emit(date);
    }else{
      const date: Date = new Date();
      date.setDate(date.getDate() + 1);
      this.emitSaveDate.emit(date);
    }
  }

  onRemoveDate(): void{ this.emitRemoveDate.emit(true);}

  onCloseDate(): void{ this.emitShowDate.emit(false);}
}
