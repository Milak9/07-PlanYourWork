import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-card-attachment',
  templateUrl: './card-attachment.component.html',
  styleUrls: ['./card-attachment.component.css']
})
export class CardAttachmentComponent implements OnInit {

  @Output()
  emitShowAttachment: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  emitSendFiles: EventEmitter<FileList> = new EventEmitter<FileList>();

  constructor() {}

  ngOnInit(): void {}

  onCloseDate(): void{ this.emitShowAttachment.emit(false);}

  handleFileInput(files: FileList): void { this.emitSendFiles.emit(files);}
}
