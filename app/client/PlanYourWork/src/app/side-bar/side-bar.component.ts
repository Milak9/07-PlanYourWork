import { Project } from './../models/project.model';
import { BoardModel } from './../models/board.model';
import { BoardService } from './../services/board.service';
import { Component, Input, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { trigger, transition, style, animate, query, stagger } from '@angular/animations';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ProfileService } from '../services/profile.service';

export const fadeAnimation = trigger('fadeAnimation', [
  transition('* <=> *', [
    query(':enter',
      [style({ opacity: 0 }), stagger('150ms', animate('1000ms ease-out', style({ opacity: 1 })))],
      { optional: true }
    ),
    query(':leave',
      animate('500ms', style({ opacity: 0 })),
      { optional: true }
    )
  ])
]);

@Component({
  selector: 'app-side-bar',
  templateUrl: './side-bar.component.html',
  styleUrls: ['./side-bar.component.css'],
  animations: [fadeAnimation]
})

export class SideBarComponent implements OnInit {

  showProjectBoards: boolean = false;
  showParticipants: boolean = false;
  public boards: BoardModel[] = [];
  private activeSubscriptions: Subscription[] = [];
  public users: String[] = [];

  @Input()
  projectId: string;
  @Input()
  currentBoardId:string;

  constructor(public boardService: BoardService,
              public authService: AuthService,
              private router:Router,
              private profileService:ProfileService,
              private activatedRoute: ActivatedRoute) {

    const params = this.activatedRoute.paramMap.subscribe(params => {
      this.projectId = params.get('projectId');
    });

    this.activeSubscriptions.push(params);
    const sub =  this.boardService.boards.subscribe((data)=>{
      this.boards=data;
    });

      this.activeSubscriptions.push(sub);

    const projSub = this.profileService.getProjectById(this.projectId).subscribe((project: Project) => {

      this.users = project.team;

    });

    this.activeSubscriptions.push(projSub);

    const subBoard = this.boardService.addBoardSubject.subscribe((board: BoardModel) => {
      this.boards.push(board);
    });

    this.activeSubscriptions.push(subBoard);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.activeSubscriptions.forEach(sub => sub.unsubscribe());
  }

  openNav() {
    document.getElementById("sideNav").style.width = "400px";
  }

  closeNav() {
    document.getElementById("sideNav").style.width = "0";
  }

  openBoards() {

    this.showProjectBoards = !this.showProjectBoards;
  }
  goBacktoProfile(){
    let subGoToProfile = this.profileService.getDiffrentUser(this.authService.sendUserDataIfExists().username).subscribe();
    this.activeSubscriptions.push(subGoToProfile);
    this.router.navigate(['/profile',this.authService.sendUserDataIfExists().username]);
  }

  openParticipants() {
    this.showParticipants = !this.showParticipants;
  }
  openUser(user: string) {
    this.router.navigate(['/profile', user]);

    let subGoToProfile = this.profileService.getDiffrentUser(user).subscribe();
    this.activeSubscriptions.push(subGoToProfile);
  }
}
