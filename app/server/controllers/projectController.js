const Project = require('../models/projectModel');
const Board = require('../models/boardModel');
const mongoose = require('mongoose');

module.exports.getProjects = async (req, res, next) => {
    try{
        const projects = await Project.find({}).exec();
        res.status(200).json(projects);
    }catch(err){
        next(err);
    }
};
module.exports.deleteAllProjectsFromAUser = async (req, res, next) => {
    try{
        const deleteUser = req.params.username;
        const projects = await Project.find({"team":{ "$elemMatch" : { "$eq": deleteUser } }}).exec();
  
        let deletedPro=0;
        if(projects){                                
            for (let project of projects){
                const index = project.team.findIndex((user) => user===deleteUser);
                project.team.splice(index, 1);

                deletedPro=deletedPro+1;
                if(project.team.length === 0){
                    await Project.findByIdAndDelete(project.id)
                }
            else{
                await project.save();
            }
        }
        next();
    }
        else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};
module.exports.getProjectById = async (req, res, next) =>{

    try{
        
        const project = await Project.findById(req.params.project_id).exec();
        
        if(project){                                
            res.status(200).json(project);
        }
        else{
            res.status(404).send();
        }
    }
    catch(err){
        next(err);
    }
}
module.exports.getTeamMembers = async (req, res, next) =>{

    try{
        const project = await Project.findById(req.params.project_id).exec();
        
        if(project){
            req.body.team = project.team;
            next();
        }
        else{
            res.status(404).send();
        }
    }
    catch(err){
        next(err);
    }
}

module.exports.getDescription = async (req, res, next) =>{

    try{
        
        const project = await Project.findById(req.params.project_id).exec();
        
        if(project){              
            if(project.description){
                res.status(200).json(project.description);
            }
            else{
                res.status(200).json("Empty");
            }                  
            
        }
        else{
            res.status(404).send();
        }
    }
    catch(err){
        next(err);
    }
}

module.exports.getTitle = async (req, res, next) =>{

    try{
        
        const project = await Project.findById(req.params.project_id).exec();
        
        if(project){              
            
            res.status(200).json(project.title);
            
        }
        else{
            res.status(404).send();
        }
    }
    catch(err){
        next(err);
    }
}
module.exports.getBoards = async (req, res, next) => {

    try{
        const project = await Project.findById(req.params.project_id).exec();
        let boards = [];
        
        if(project){              
            for (let id of project.boards){
                const board = await Board.findById(id);
                boards.push(board);
            }
            res.status(200).json(boards);
            
        }
        else{
            res.status(404).send();
        }
    }
    catch(err){
        next(err);
    }
}
module.exports.createProject = async (req, res, next) => {
    if(!req.body.title || !req.body.team){
        res.status(400).send("You must create team for project and name it.");
    }else{
        try{
            const newProject = new Project({
                _id: new mongoose.Types.ObjectId(),
                title: req.body.title,
                description: req.body.description,
                team: req.body.team,
                boards: [req.body.newBoard._id]
            });

            req.body.newProject = newProject;
            await newProject.save();
            next()
        }catch(err){
            next(err);
        }
    }
};

module.exports.updateTitleOrDescription = async (req, res, next) => {
    try{
        const project = await Project.findById(req.params.project_id).exec();

        if(project){
            const body = req.body;
            if(body.title){
                project.title = body.title;
            }
            if(body.description){
                project.description = body.description;
            }
            await project.save();
            next();
        
        }else{
            res.status(404).send();
        } 
    }catch(err){
        next(err);
    }   
};

module.exports.deleteProjectById = async (req, res, next) => {
    try{
        id = req.params.id ? req.params.id : req.params.project_id;
        
        const project  = await Project.findById(id).exec();
        if(project){
            if(project.team.length === 0 || project.boards.length===0){
                await Project.findByIdAndDelete(id)
            }
            if(req.body.accessToken){
                    res.status(200).send({accessToken:req.body.accessToken}); 
            }
            else{
             res.status(200).send();
            }
            
        }
        else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};

module.exports.deleteTeamMembers = async (req, res, next) => {
    try{
        if(req.body.team_delete){
            const project  = await Project.findById(req.params.project_id).exec();
            const body = req.body;
            if(project){
                let deleteMembers = [];
                if(body.team_delete){
                    deleteMembers = body.team_delete;
                }
                else{
                    deleteMembers = body.team;
                }
       
                for(member of deleteMembers){
                    const index = project.team.findIndex((user) => user===member);
                    project.team.splice(index, 1);
                }
                await project.save();
            }else{
                res.status(404).send();
            }
          
          
    }
    next();  
    } catch(err){
        next(err);
    }
};

module.exports.addTeamMember = async (req, res, next) => {
    try{
        if(req.body.team_add){
        const project  = await Project.findById(req.params.project_id).exec();
        const body = req.body;
        if(project){
            let team = [];
            team = body.team_add ? body.team_add : body.team;
            if(team.length!==0){
                for(let member of team){
                    const index = project.team.findIndex((user) => user===member);
                    if(index===-1){
                        project.team.push(member);
                    }
                    else{
                        res.status(400).send({ message: "User " + team[index] + " is already in the project!" });
                    }
                }
                await project.save();  
            }
            
        }else{
            res.status(404).send();
        }
    }
    next();
    }catch(err){
        next(err);
    }
};
module.exports.addBoard = async (req, res, next) => {
    try{
        const project  = await Project.findById(req.body.projectId).exec();
        
        const body = req.body;

        if(project){
                if(project.boards.find(board => board === body.newBoard._id)===undefined){
                    project.boards.push(body.newBoard._id);
                    await project.save();
                    res.status(200).send(body.newBoard);    
                }
                else{
                    res.status(400).send("Board already exists");
                }
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};

module.exports.removeBoard = async (req, res, next) => {
    try{
        const project  = await Project.findById(req.params.project_id).exec();        
        if(project){
                    const index = project.boards.findIndex(board => board == req.params.board_id);
                    
                    if(index!==-1){
                        project.boards.splice(index, 1);
                    }
                    else {
                        res.status(400).send("Board doensn't exists");
                    }
                    await project.save();
                    if(project.boards.length===0){
                        req.body.team_delete = project.team ;
                        next();
                    }
                    else{
                        res.status(200).send();
                    }
                }
        else{
            res.status(404).send();
        }
    }
    catch(err){
        next(err);
    }
};

module.exports.deleteUserFromAllProjects = async (req, res, next) => {
    try{
        const projects  = await Project.find().exec();        

        if(projects){
            const usersProjects = projects.filter((proj) => {
                const ind = proj.team.findIndex((user) => user === req.params.username);
                if(ind !== -1){
                    return true;
                }
            });

            for(project of usersProjects){
                const newTeam = project.team.filter((user) => user!==req.params.username);
                project.team = newTeam;
                await project.save();
            }
            res.status(200).send({message:"Success!"});
        }
        else{
            res.status(404).send();
        }
    }
    catch(err){
        next(err);
    }
};