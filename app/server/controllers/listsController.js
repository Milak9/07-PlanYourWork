const List = require('../models/listModel');
const Board = require('../models/boardModel');
const Card = require('../models/cardsModel');

const mongoose = require('mongoose');

module.exports.getLists = async (req, res, next) => {
    try {
        const lists = await List.find({}).exec();
        res.status(200).json(lists);
    } catch(err) {
        next(err);
    }
};

module.exports.getListById = async (req, res, next) => {
    try {
        const list = await List.findById(req.params.listId).populate('tasks').exec();

        if (list) {
            res.status(200).json(list);
        } else {
            res.status(400).send();
        }
    } catch(err) {
        next(err);
    }
};

module.exports.createList = async (req, res, next) => {
    if (!req.body.name) {
        res.status(400).send();
    } else {
        try {
            const newList = new List({
                _id: new mongoose.Types.ObjectId(),
                name: req.body.name,
                tasks: []
            });

            await newList.save();

            res.status(201).json(newList);
        } catch(err) {
            next(err);
        }
    }
};

module.exports.updateListById = async (req, res, next) => {
    try {

        const list = await List.findById(req.params.listId).exec();

        if (list) {
            const updateListData = req.body;

            if (req.body.name)
                list.name = updateListData.name;

            if (req.body.tasks.length === 0) {
                list.tasks = [];
            }
            else if (req.body.tasks) {
                list.tasks = req.body.tasks;
            }
            await list.save();
            
            res.status(200).send();
            

        } else {
            res.status(404).send();
        }

    } catch(err) {
        next(err);
    }
};
module.exports.removeCardFromList = async (req, res, next) => {
    try {
        const list = await List.findById(req.params.listId).exec();
        if (list) {
            const index = list.tasks.findIndex(task => task == req.params.card_id);
            if(index!==-1){
                        list.tasks.splice(index, 1);
            }
            await list.save();
            next();
        }
    } catch(err) {
        next(err);
    }
};
module.exports.deleteListById = async (req, res, next) => {

    try {
        const list = await List.findById(req.params.listId).exec();

        if (list) {
            await List.findByIdAndDelete(req.params.listId).exec();
            res.status(200).send();
        } else {
            res.status(404).send();
        }

    } catch(err) {
        next(err);
    }
};

module.exports.deleteListsAndTasks = async function (req, res, next) {
    try{
        const board = await Board.findById(req.params.board_id).exec();

        if(board){
            for (listId of board.lists) {
                const list = await List.findById(listId).exec();

                if(list){
                    
                    for (cardId of list.tasks) {

                        const card = await Card.findById(cardId).exec();

                        if (card) {
                            await Card.findByIdAndDelete(cardId).exec();
                        }
                    }

                    await List.findByIdAndDelete(listId).exec();

                } else{
                    res.status(404).send();
                } 
            };
            
            next();
        }else {
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};