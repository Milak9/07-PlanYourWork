const Users = require('../models/usersModel');
const Projects = require('../models/projectModel');
const jwt_helper = require('../helpers/jwt_helper');
const createError = require('http-errors')
const mongoose = require('mongoose');
const bcrypt = require('bcrypt')
const {uploadFile} = require('./uploadController');

module.exports.getUsers = async (req, res, next) => {
    try{
        const users = await Users.find({}).exec();
        res.status(200).json(users);
    }catch(err){
        next(err);
    }
};
module.exports.getUserByUsername = async (req, res, next) => {
    try{    
        const user = await Users.findOne({username: req.params.username}).exec();
        
        if(user){
            res.status(200).json(user);
        }else{
            res.status(400).send();
        }
    }catch(err){
        next(err);
    }

};

module.exports.getProjectsOfTheUser = async (req, res, next) =>{
     try{
        
        const user = await Users.findOne({username: req.params.username}).exec();
        
        if(user){                            
            let userProjects = [];
            for (let id of user.projects){
                const proj = await Projects.findById(id);
                userProjects.push(proj);
            }
            userProjects.sort((a,b) => (a._id > b._id) ? -1 : ((b._id > a._id) ? 1 : 0));
            res.status(200).json(userProjects);
        }
        else{
            res.status(404).send();
        }
    }
    catch(err){
        next(err);
    }
}

module.exports.createUser = async (req, res, next) => {
    if(!req.body.username || !req.body.password || !req.body.email){
        res.status(400).send("Not enought informations!");
    } 
    else{

        var user = await Users.findOne({ email: req.body.email}).exec();
        if(user){
           return res.status(400).send({message: "User already exists with this email!"});
        }
        var user = await Users.findOne({ username: req.body.username}).exec();
        if(user){
          return   res.status(400).send({message : "User already exists with this username!"});
        }
        else{
        try{
            const hPass=await bcrypt.hash(req.body.password,10)
            const newUser = new Users({
                username: req.body.username,
                password: hPass,
                email: req.body.email,
                fullname:req.body.fullname
            });   
            await newUser.save();
            next();
        }catch(err){
            next(err);
        }
    }
    
        
    
    }
};

module.exports.loginUser = async (req, res, next) => {
    if(!req.body.email || !req.body.password){
        res.status(400).send();

    }else{
        try{
                       
            var user = await Users.findOne({ email: req.body.email }).exec();
            if(!user) {
                return res.status(400).send({ message: "User with this email doesn't exists!" });
            }
            if(!bcrypt.compareSync(req.body.password, user.password)) {
                return res.status(400).send({ message: "The password is invalid" });
            }
            const accessToken = await jwt_helper.signAccessToken(user,true)
            const refreshToken = await jwt_helper.signAccessToken(user,false)
           
            res.status(201).json({accessToken:accessToken,refreshToken:refreshToken});     
        }catch(err){
            next(err);
        }
    }
};
module.exports.refreshToken = async (req, res, next) => {
    try {
      const refreshToken  = req.body.reftoken
      if (!refreshToken){ 
      throw createError.BadRequest()
      }
      const user = await jwt_helper.verifyRefreshToken(refreshToken,false)
      if(user){
      const accessToken = await jwt_helper.signAccessToken(user,true);
      res.send({ accessToken: accessToken})
      }
      else {
        res.send({ message:"RT"})
      }
    } catch (error) {
      next(error)
    }



},
module.exports.logoutUser = async (req, res, next) => {
    try {
      const  refreshToken = req.body.reftoken
      if (!refreshToken) throw createError.BadRequest()
      const user = await jwt_helper.verifyRefreshToken(refreshToken)
      res.status(204).send({ loggedOutUser:  user._id })   
    } catch (error) {
      next(error)
    }
  }
module.exports.updateUserByUsername = async (req, res, next) => {
    try{
        const user = await Users.findOne({username: req.params.id}).exec();
        let changed;
        if(user){
            const updateUser = req.body;
                if(req.body.oldPass && req.body.newPass){
                    if(!bcrypt.compareSync(updateUser.oldPass, user.password)){
                        res.status(400).send({ message: "Wrong current pasword!" });
                    }
                    else{
                        const hPass=await bcrypt.hash(updateUser.newPass,10);
                        user.password = hPass;
                        changed = "Password";
                    }
                }

                else if(req.body.newEmail){
                    user.email = updateUser.newEmail;
                    changed = "Email";
                }
                else if(req.body.newFullname){
                    user.fullname = updateUser.newFullname;
                    changed = "Name";
                }
                else {
                    return res.status(400).send({ message: "Body of the request is invalid!" });
                }
                await user.save();
                const accessToken = await jwt_helper.signAccessToken(user,true)
                res.status(200).send({accessToken:accessToken,message: changed + " succusfully changed!"});
                
        }else{
            res.status(404).send();
        } 
    }catch(err){
        next(err);
    }   
};
module.exports.deleteUserByUsername = async (req, res, next) => {
    try{
        const user = await Users.findOne({username : req.params.username}).exec();

        if(user){
            await Users.deleteOne({username : req.params.username}).exec();
            next();
        }
    }catch(err){
        next(err);
    }
};

module.exports.joinProject = async (req,res,next) => {
    try {
        const team = req.body.team ? req.body.team : req.body.team_add;
        if(team){
            newProject = req.body.newProject;
            const project_id = req.params.project_id ? req.params.project_id : newProject._id;
            const n = team.length
            for (let index = 0; index < n; index++) {
                const username = team[index];
                const user = await Users.findOne({ username: username}).exec();
                if (user){
             
                    const index = user.projects.findIndex(project => project == project_id);
                    if(index===-1 || !user.projects){
                    user.projects.push(project_id);
                    await user.save();
                    if(user.username === req.body.loggedInUser){
                        const accessToken = await jwt_helper.signAccessToken(user,true);
                        req.body.accessToken = accessToken;
                    }
                    }        
                }
                else {
                    res.status(200).send({message:"Missing user " + username});
                }
            }
            if(!req.params.project_id){
                res.status(200).send({"project_id": newProject._id, "board_id": req.body.newBoard._id,accessToken:req.body.accessToken});
            }
            else{
                res.status(200).send({"project_id":req.params.project_id });
            }
        }
        else{
            res.status(200).send();
        }      
    } catch(err) {
        next(err)
    }
};
module.exports.checkIfUsersExist = async (req, res, next) => {
   try {
    let team;
    if(req.body.team_add || req.body.team){
            team = req.body.team_add ? req.body.team_add  :  req.body.team;
    }
    else if(req.body.team_delete){
            team = req.body.team_delete;
    }
    if(team){
    const n = team.length
    for (let index = 0; index < n; index++) {
        const username = team[index];
        const user = await Users.findOne({ username: username}).exec();
        if(!user){
            res.status(404).send({ message: "User " + team[index] + " isn't registered!" });
            return;
        }
    }
   } 
    next();
  } catch(err) {
       next(err)
    }
};

module.exports.leaveProject = async (req, res, next) => {
    try {
        const user = await Users.findOne({username : req.params.username}).exec();

        if (user) {

            const index = user.projects.findIndex(project => project == req.params.project_id);

            if (index === -1) {
                res.status(404).send();
            } else {
                user.projects.splice(index, 1);
                await user.save();
                res.status(200).send();
            }

        } else {
            res.status(404).send();
        }

    } catch(err) {
        next(err);
    }
}
module.exports.removeProjectForMultipleUsers = async (req, res, next) =>{
    try{
        if(req.body.team_delete){
        const team = req.body.team_delete;

        for(member of team){
            let user =  await Users.findOne({username : member});
            if(user){
                const index = user.projects.findIndex(board => board == req.params.project_id);
                if(index!==-1){
                    user.projects.splice(index, 1);
                }
                await user.save();
                if(user.username === req.body.loggedInUser){
                    const accessToken = await jwt_helper.signAccessToken(user,true);
                    req.body.accessToken = accessToken;
                } 
            }
            else{
                res.status(404).send({message : user + " not found"});
            }
        }
    }
    next();

    }catch(error){
        next(error);
    }
}

module.exports.changeUserProfileImage = async (req, res, next) =>{
    try {
        const user = await Users.findOne({username : req.params.username}).exec();
        if(user){
            await uploadFile(req, res);

            if (req.file == undefined) {
                const error = new Error('Please upload a file!');
                error.status = 400;
                throw error;
            }

            const imgUrl = req.file.filename;
            
            user.imgUrl = imgUrl;
            await user.save();

            const accessToken = await jwt_helper.signAccessToken(user,true);
            const refreshToken = await jwt_helper.signAccessToken(user,false);  

            res.status(200).send({accessToken:accessToken,refreshToken:refreshToken});
        }

        
    } catch (err) {
        next(err);
    }
}

module.exports.fetchUsersInfo =  async (req, res, next) => {

    try{
        let body = [];
        const team = req.body.team;
        for(member of team){
            const user = await Users.find({username : member}).exec();
            if(user){
                body.push({/*image : user.imgUrl,*/ name: user[0].fullname + " (" + member + ")"});    
            }
            else{
                res.status(404).send({message : "user " + member + "not found"});
            }
            
        }
        res.status(200).send(body);
    }
    catch(err){
        next(err);
    }
}
