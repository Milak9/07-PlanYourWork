const Card = require('../models/cardsModel');
const List = require('../models/listModel');
const mongoose = require('mongoose');
const { uploadFile } = require('./uploadController');
const fs = require('fs')

module.exports.getCards = async (req, res, next) => {
    try{
        const cards = await Card.find({}).exec();
        res.status(200).json(cards);
    }catch(err){
        next(err);
    }
};

module.exports.getCardById = async (req, res, next) => {
    try{    
        const card = await Card.findById(req.params.id)
                               .populate('checklists')
                               .exec();
        if(card){
            res.status(200).json(card);
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }

};

module.exports.createCard = async (req, res, next) => {
    if( !req.body.title || !req.body.listId){
        res.status(400).send();
    }else{
        try{
            const newCard = new Card({
                _id: new mongoose.Types.ObjectId(),
                listId: req.body.listId,
                title: req.body.title
            });   

            await newCard.save();

            res.status(201).json(newCard);
        }catch(err){
            next(err);
        }
    }
};

module.exports.updateCardById = async (req, res, next) => {
    try{
        const card = await Card.findById(req.params.id).exec();

        if(card){
            const updateCard = req.body;
            
            if (updateCard.listId) {
                card.listId = updateCard.listId;
            }
            else if(updateCard.title){
                card.title = updateCard.title;

            }else if(updateCard.description){
                card.description = updateCard.description;

            }else if(updateCard.members){
                card.members = updateCard.members;

            }else if(updateCard.cover){
                card.cover = updateCard.cover;
                
            }else if(updateCard.labels){
                card.labels = updateCard.labels;

            }else if(updateCard.checklist){
                card.checklists.push(updateCard.checklist._id);

            }else if(updateCard.dueDate){
                card.dueDate = updateCard.dueDate;
                card.completionStatus = updateCard.completionStatus;
                
            }else if(updateCard.removeDueDate){
                card.dueDate = null;
                card.completionStatus = updateCard.completionStatus;

            }else if(updateCard.fileIndex >= 0){
                const path = __uploadDir + '/' + card.fileUrls[updateCard.fileIndex];
                card.fileUrls.splice(updateCard.fileIndex, 1);
                fs.unlink(path, (err) => {
                    if (err) {
                      console.error(err);
                      return;
                    }
                  })
            }else if(updateCard.updateAttachmentVisability){
                card.hideAttachments = !card.hideAttachments;
                if(card.hideAttachments){
                    card.hideAttachmentsText = 'View attachments (' + card.fileUrls.length.toString() + ')';
                }else{
                    card.hideAttachmentsText = 'Hide attachments';
                }
            }else{
                return res.status(400).send({ message: "This request is not valid!" });
            }

            await card.save();
            res.status(200).send();
            
        }else{
            res.status(404).send();
        } 
    }catch(err){
        next(err);
    }   
};

module.exports.putCardFile = async function (req, res, next) {
    try {
        const card = await Card.findById(req.params.id).exec();

        await uploadFile(req, res);
    
        if (req.file == undefined) {
            const error = new Error('Please upload a file!');
            error.status = 400;
            throw error;
        }
    
        const imgUrl = req.file.filename;
        card.fileUrls.push(imgUrl);
        await card.save();
    
        res.status(200).send({
            message: 'Uploaded the file successfully: ' + req.file.originalname,
        });
    } catch (err) {
            next(err);
    }
};

module.exports.deleteCardById = async (req, res, next) => {
    try{
        const card = await Card.findById(req.params.card_id).exec();
        if(card){
            await Card.findByIdAndDelete(req.params.card_id).exec();
            res.status(200).send();
        }else{
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};

module.exports.deleteChecklist = async function (req, res, next) {
    try {
        const card = await Card.findById(req.params.id).exec();

        if(card && req.body.checklistId){
            card.checklists.filter((id) => id!==req.body.checklistId);
            await card.save();
            res.status(200).send();
            next();
        }
    } catch (err) {
            next(err);
    }
};

module.exports.deleteCards = async function (req, res, next) {
    try{
        const list = await List.findById(req.params.listId).exec();

        if(list){
            for (cardId of list.tasks) {
                const card = await Card.findById(cardId).exec();

                if(card){
                    await Card.findByIdAndDelete(cardId).exec();
                }else{
                    res.status(404).send();
                }

            };

            if (req.body.deleteByBoard)
                return;

            next(); 

        }else {
            res.status(404).send();
        }
    }catch(err){
        next(err);
    }
};