const express = require('express');
const router = express.Router();
const controller = require('../../controllers/projectController');
const userController = require('../../controllers/usersController');
const boardController = require('../../controllers/boardController');
const listController = require('../../controllers/listsController');
const jwt_helper = require('../../helpers/jwt_helper');
//http://localhost:5000/projects
router.get('/', controller.getProjects);
//http://localhost:5000/projects/:project_id
router.get('/:project_id', jwt_helper.verifyAccessToken, controller.getProjectById);
//http://localhost:5000/projects/:project_id/team
router.get('/:project_id/team',jwt_helper.verifyAccessToken, controller.getTeamMembers, userController.fetchUsersInfo);

//http://localhost:5000/projects/:project_id/description
router.get('/:project_id/description', jwt_helper.verifyAccessToken,controller.getDescription);
//http://localhost:5000/projects/:project_id/title
router.get('/:project_id/title', jwt_helper.verifyAccessToken, controller.getTitle);
//http://localhost:5000/projects/:project_id/boards
router.get('/:project_id/boards',jwt_helper.verifyAccessToken, controller.getBoards);
//http://localhost:5000/projects/
router.post('/', jwt_helper.verifyAccessToken, userController.checkIfUsersExist, boardController.createBoard, controller.createProject, userController.joinProject);
//http://localhost:5000/projects/:project_id
router.put('/:project_id', jwt_helper.verifyAccessToken,
                   userController.checkIfUsersExist,    
                   controller.updateTitleOrDescription, 
                   userController.removeProjectForMultipleUsers, 
                   controller.deleteTeamMembers,
                   controller.addTeamMember,
                   userController.joinProject);
//http://localhost:5000/projects/:project_id/team/delete_member
router.put('/:project_id/team/delete_member',jwt_helper.verifyAccessToken,userController.removeProjectForMultipleUsers, controller.deleteTeamMembers, controller.deleteProjectById);
// //http://localhost:5000/projects/:project_id/team/add_member
// router.put('/:project_id/team/add_member', jwt_helper.verifyAccessToken, controller.addTeamMember);
//http://localhost:5000/projects/:project_id/boards/add_board
router.post('/:id', jwt_helper.verifyAccessToken,boardController.createBoard, controller.addBoard);
//http://localhost:5000/projects/:project_id/:board_id
router.delete('/:project_id/:board_id', jwt_helper.verifyAccessToken, listController.deleteListsAndTasks, boardController.deleteBoardById, controller.removeBoard,  userController.removeProjectForMultipleUsers,controller.deleteProjectById);
//http://localhost:5000/projects/:project_id
router.delete('/:project_id', jwt_helper.verifyAccessToken, controller.deleteProjectById)

module.exports = router;
