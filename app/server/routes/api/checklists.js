const express = require('express');
const router = express.Router();
const jwt_helper = require('../../helpers/jwt_helper');
const controller =  require('../../controllers/checklistsController');

// http://localhost:5000/api/checklists
router.get('/', controller.getChecklists);

// http://localhost:5000/api/checklists/objectId
router.get('/:id',jwt_helper.verifyAccessToken, controller.getChecklistById)

// http://localhost:5000/api/checklists
router.post('/',jwt_helper.verifyAccessToken, controller.createChecklist);

// http://localhost:5000/api/checklists/objectId
router.patch('/:id', jwt_helper.verifyAccessToken,controller.updateChecklistById);

// http://localhost:5000/api/checklists/objectId
router.delete('/:id', jwt_helper.verifyAccessToken,controller.deleteChecklistById);
// http://localhost:5000/api/checklists/objectId/add_item
router.post('/:id/add_item',  jwt_helper.verifyAccessToken,controller.addItem);

// http://localhost:5000/api/checklists/objectId/delete_item
router.patch('/:id/delete_item', jwt_helper.verifyAccessToken, controller.deleteItem);

// http://localhost:5000/api/checklists/objectId/check_item
router.patch('/:id/update_checked', jwt_helper.verifyAccessToken, controller.checkItem);

// http://localhost:5000/api/checklists/objectId/hide_items
router.patch('/:id/hide_items', jwt_helper.verifyAccessToken, controller.hideItems);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': `Unknown request ${unknownMethod} to ${unknownPath}`});
});
module.exports = router;