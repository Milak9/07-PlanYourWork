const express = require('express');
const router = express.Router();
const jwt_helper = require('../../helpers/jwt_helper');
const controller = require('../../controllers/listsController');
const boardsController = require('../../controllers/boardController');
const cardsController = require('../../controllers/cardsController');

// http://localhost:5000/api/list
router.get('/', controller.getLists);

// http://localhost:5000/api/list/listId
router.get('/:listId',jwt_helper.verifyAccessToken,  controller.getListById)

// http://localhost:5000/api/list
router.post('/', jwt_helper.verifyAccessToken,  controller.createList);

// http://localhost:5000/api/list/listId
router.put('/:listId',jwt_helper.verifyAccessToken,  controller.updateListById);

// http://localhost:5000/api/list/boardId/listId
router.delete('/:boardId/:listId', jwt_helper.verifyAccessToken, boardsController.deleteListInBoard, cardsController.deleteCards, controller.deleteListById);

router.use((req, res, next) => {
    const unknownMethod = req.method;
    const unknownPath = req.path;
    res.status(500).json({'message': `Unknown request ${unknownMethod} to ${unknownPath}`});
});

module.exports = router;