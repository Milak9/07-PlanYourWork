const mongoose = require('mongoose');

const usersShema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    fullname: {
        type: String,
        required: true
    },
    projects: {
        type: [mongoose.Schema.Types.ObjectId],
        required: false,
        default: []
    },
    imgUrl: {
        type: mongoose.Schema.Types.String,
        required: true,
        default: 'default-user.png',
      }
});

const usersModel = mongoose.model('users', usersShema);

module.exports = usersModel;