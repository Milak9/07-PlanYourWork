const mongoose = require('mongoose');

const cardsSchema = new mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    listId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'list',
        required: true
    },
    title: {
        type: String,
        required: true
    },
    description: {
        type: String
    },
    members: {
        type: [String],
        default: []
    },
    labels: {
        type: [String],
        default: []
    },
    checklists:{
        type: [mongoose.Schema.Types.ObjectId],
        ref: 'checklists'
    },
    dueDate: Date,
    completionStatus: String,
    fileUrls: {
        type: [String],
        default: []
    },
    hideAttachments: {
        type: Boolean,
        default: false
    },
    hideAttachmentsText: {
        type: String,
        default: 'Hide attachments'
    },
    cover: {
        type: String,
        default: 'none'
    }
});

const cardsModel = mongoose.model('cards', cardsSchema);

module.exports = cardsModel;