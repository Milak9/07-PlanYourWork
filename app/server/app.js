const path = require('path');
const fs = require('fs');

global.__uploadDir = path.join(__dirname, 'resources', 'uploads');

if (!fs.existsSync(__uploadDir)) {
    fs.mkdirSync(__uploadDir);
}

const express = require('express');
const mongoose = require ('mongoose');
var cors = require('cors')
const bodyParser = require('body-parser')

const app = express();
app.use(cors())
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.use(express.json());

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Content-Type');
  
    if (req.method === 'OPTIONS') {
      res.header(
        'Access-Control-Allow-Methods',
        'OPTIONS, GET, POST, PATCH, PUT, DELETE'
      );
  
      return res.status(200).json({});
    }
  
    next();
  });
const databaseString = "mongodb://localhost:27017/PlanYourWorkDB";
mongoose.connect(databaseString, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true 
});

mongoose.connection.once('open', function(){
    console.log('Successfully connected to MongoDB!');
});

mongoose.connection.on('error', error => {
    console.log('Error: ', error);
});

const cardsAPI = require('./routes/api/cards');
app.use('/api/cards', cardsAPI);

const checklistsAPI = require('./routes/api/checklists');
app.use('/api/checklists', checklistsAPI);

const projectAPI = require('./routes/api/project');
app.use('/projects', projectAPI);

const usersAPI = require('./routes/api/users');
app.use('/api/users', usersAPI);

const boardAPI = require('./routes/api/board');
app.use('/api/board', boardAPI);

const listAPI = require('./routes/api/lists');
app.use('/api/list', listAPI);

app.use(express.static(__uploadDir));

app.use(function (req, res, next){
    const error = new Error('Request is not supported');
    error.status = 405;

    next(error);
});
 
app.use(function (error, req, res, next){
    const statusCode = error.status || 500;
    res.status(statusCode).json({
        error: {
            message: error.message,
            status: statusCode,
            stack: error.stack,
        },
    });
});

module.exports = app;
